from datetime import datetime as dt


DATE_MAX = dt(8888, 12, 31)

JPN_HOLIDAY_CALENDAR_URL = "ja.japanese#holiday@group.v.calendar.google.com"
JSL_HOLIDAY_CALENDAR_URL = "jslhw2app@gmail.com"


class CleaningStatus():
    CLEANING_START = 1          # 掃除開始時間になった時
    CLEANING_END = 2            # 掃除完了ボタンを押した時
    CLEANING_CHANGE = 3         # 掃除当番が変わった時
    CLEANING_DATE_CHANGE = 4    # 掃除日が変わった時
    CLEANING_PREPARATION = 5    # その他(当日の昼礼時等)


def str_to_datetime(string, datetime_format='%Y-%m-%d'):
    """文字列から日付に変換"""

    return dt.strptime(string, datetime_format)


def datetime_to_str(datetime, datetime_format='%Y-%m-%d'):
    """日付から文字列に変換"""

    return datetime.strftime(datetime_format)

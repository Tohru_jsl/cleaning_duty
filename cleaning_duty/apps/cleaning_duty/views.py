from django.contrib.auth.decorators import login_required
from django.http.response import JsonResponse
from django.shortcuts import render
from django.views.decorators.http import require_http_methods

from cleaning_duty.apps.cleaning_duty.query import *
from cleaning_duty.apps.calendar import Calendar
from cleaning_duty.apps.utils import CleaningStatus, datetime_to_str, str_to_datetime


@login_required
def cleaning_duty(request):
    """メイン画面"""

    next_cleaning_date = datetime_to_str(get_next_cleaning_date().next_cleaning_date, '%Y-%m-%d')
    jsl_holidays = Calendar.get_holiday(JSL_HOLIDAY_CALENDAR_URL)
    context = {
        'next_cleaning_date': next_cleaning_date,
        'jsl_holidays': jsl_holidays,
    }

    return render(request, 'main/main.html', context=context)


@login_required
@require_http_methods(['GET'])
def get_cleaning_duty_list(request):
    """掃除当番表を取得"""

    return JsonResponse({
        'data': create_cleaning_duty_list()
    })


@login_required
@require_http_methods(['POST'])
def save_cleaning_duty(request):
    """掃除当番を保存"""

    change_cleaning_duty_id = int(request.POST['change_cleaning_duty_id'])
    change_duty_status = int(request.POST['change_duty_status'])
    relief_cleaning_duty_id = request.POST.get('relief_cleaning_duty_id')
    if relief_cleaning_duty_id:
        relief_cleaning_duty_id = int(relief_cleaning_duty_id)
    is_slack_alert = request.POST['is_slack_alert']

    # 掃除当番が正常に更新されたか
    data_status = change_cleaning_duty(change_cleaning_duty_id, change_duty_status, relief_cleaning_duty_id)
    if is_slack_alert == 'true':
        send_message_to_slack(CleaningStatus.CLEANING_CHANGE)

    return JsonResponse({
        'data': data_status
    })


@login_required
@require_http_methods(['POST'])
def save_next_cleaning_date(request):
    """次回掃除実施日を保存"""

    next_cleaning_date = request.POST['next_cleaning_date']
    next_cleaning_date = str_to_datetime(next_cleaning_date, '%Y-%m-%d')
    is_slack_alert = request.POST['is_slack_alert']

    next_cleaning_date_table = get_next_cleaning_date()
    next_cleaning_date_table.next_cleaning_date = next_cleaning_date
    next_cleaning_date_table.save()

    if is_slack_alert == 'true':
        send_message_to_slack(CleaningStatus.CLEANING_DATE_CHANGE)

    return JsonResponse({
        'data': True
    })


@login_required
@require_http_methods(['GET'])
def get_next_cleaning_date_str(request):
    """次回掃除実施日を取得"""

    return JsonResponse({
        'data': datetime_to_str(get_next_cleaning_date().next_cleaning_date, '%Y-%m-%d')
    })


@login_required
@require_http_methods(['POST'])
def end_of_cleaning(request):
    """掃除完了及び次回掃除の設定"""

    cleaning_date = request.POST['cleaning_date']
    cleaning_date = str_to_datetime(cleaning_date, '%Y-%m-%d')
    is_slack_alert = request.POST['is_slack_alert']

    save_last_cleaning_date(cleaning_date)
    set_cleaning_duty()
    set_next_cleaning_date(cleaning_date)

    if is_slack_alert == 'true':
        send_message_to_slack(CleaningStatus.CLEANING_END)

    return JsonResponse({
        'data': True
    })


@login_required
@require_http_methods(['POST'])
def save_employee(request):
    """社員情報を保存"""

    user_id = int(request.POST['user_id'])
    employee_number = request.POST['employee_number']
    employee_last_name = request.POST['employee_last_name']
    employee_first_name = request.POST['employee_first_name']
    last_cleaning_date = request.POST['last_cleaning_date']
    last_cleaning_date = str_to_datetime(last_cleaning_date, '%Y-%m-%d')

    update_employee(user_id, employee_number, employee_last_name, employee_first_name, last_cleaning_date)

    return JsonResponse({
        'data': True
    })


@login_required
@require_http_methods(['GET'])
def get_newest_cleaning_date(request):
    return JsonResponse({
        'data': CleaningDuty.objects.all().order_by('-last_cleaning_date').first().last_cleaning_date
    })

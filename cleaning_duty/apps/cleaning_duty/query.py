import datetime
import slackweb

from django.db import transaction

from cleaning_duty.apps.calendar import Calendar
from cleaning_duty.apps.utils import JPN_HOLIDAY_CALENDAR_URL, JSL_HOLIDAY_CALENDAR_URL, CleaningStatus,\
    DATE_MAX, datetime_to_str
from cleaning_duty.settings import SLACK_CHANNEL
from cms.models.models import CleaningDuty, NextCleaningDate
from django.contrib.auth.models import User


@transaction.atomic()
def get_next_cleaning_date():
    """次回掃除実施日テーブルを取得"""

    next_cleaning_dates = NextCleaningDate.objects.all().order_by('id')

    if next_cleaning_dates:
        next_cleaning_date = next_cleaning_dates[0]
    else:
        next_cleaning_date = NextCleaningDate()
        next_cleaning_date.save()

    return next_cleaning_date


def create_cleaning_duty_list():
    """掃除当番表を作成"""
    cleaning_duty_list = []

    cleaning_dutys = CleaningDuty.objects.prefetch_related('fk_user').all()
    for cleaning_duty in cleaning_dutys:
        cleaning_duty_contents = {
            "user_id": cleaning_duty.fk_user.pk,
            "cleaning_duty_id": cleaning_duty.pk,
            "employee_number": cleaning_duty.fk_user.username,
            "employee_last_name": cleaning_duty.last_name,
            "employee_first_name": cleaning_duty.first_name,
            "employee_full_name": cleaning_duty.get_employee_name(),
            "last_cleaning_date": cleaning_duty.last_cleaning_date,
            "duty_status": cleaning_duty.duty_status,
        }
        cleaning_duty_list.append(cleaning_duty_contents)

    return cleaning_duty_list


@transaction.atomic()
def change_cleaning_duty(change_cleaning_duty_id, change_duty_status, relief_cleaning_duty_id=None):
    """掃除当番を変更"""

    try:
        cleaning_duty = CleaningDuty.objects.get(pk=change_cleaning_duty_id)
        cleaning_duty.duty_status = change_duty_status
        cleaning_duty.save()

        # 代わりの当番がいる場合
        if relief_cleaning_duty_id:
            cleaning_duty = CleaningDuty.objects.get(pk=relief_cleaning_duty_id)
            cleaning_duty.duty_status = CleaningDuty.DUTY
            cleaning_duty.save()
    except CleaningDuty.DoesNotExist:
        return False

    return True


@transaction.atomic()
def save_last_cleaning_date(cleaning_date):
    """前回当番担当日を更新"""

    cleaning_dutys = CleaningDuty.objects.filter(duty_status__in=[CleaningDuty.DUTY, CleaningDuty.DAY_OFF])
    for cleaning_duty in cleaning_dutys:
        if cleaning_duty.duty_status == CleaningDuty.DUTY:
            cleaning_duty.last_cleaning_date = cleaning_date
        cleaning_duty.duty_status = CleaningDuty.NONE
        cleaning_duty.save()


@transaction.atomic()
def set_cleaning_duty():
    """掃除当番を設定"""

    cleaning_dutys_datemax = CleaningDuty.objects.filter(duty_status=CleaningDuty.NONE, last_cleaning_date=DATE_MAX)
    for cleaning_duty_datemax in cleaning_dutys_datemax:
        cleaning_duty_datemax.duty_status = CleaningDuty.DUTY
        cleaning_duty_datemax.save()

    cleaning_dutys =\
        CleaningDuty.objects.all()\
            .order_by('duty_status', 'last_cleaning_date', 'fk_user__username')[:(4 - len(cleaning_dutys_datemax))]
    for cleaning_duty in cleaning_dutys:
        cleaning_duty.duty_status = CleaningDuty.DUTY
        cleaning_duty.save()


@transaction.atomic()
def set_next_cleaning_date(cleaning_date):
    """次回掃除実施日を設定"""

    next_cleaning_date = cleaning_date + datetime.timedelta(days=14)

    # 各休日を取得
    jpn_holidays = Calendar.get_holiday(JPN_HOLIDAY_CALENDAR_URL)
    jsl_holidays = Calendar.get_holiday(JSL_HOLIDAY_CALENDAR_URL)
    holidays = jpn_holidays + jsl_holidays

    # 次回掃除実施日が休日と被る場合は1日前倒し
    holidays.sort(reverse=True)
    for holiday in holidays:
        if holiday == datetime_to_str(next_cleaning_date):
            next_cleaning_date = next_cleaning_date - datetime.timedelta(days=1)

    next_cleaning_date_table = get_next_cleaning_date()
    next_cleaning_date_table.next_cleaning_date = next_cleaning_date
    next_cleaning_date_table.save()


def update_employee(user_id, employee_number, employee_last_name, employee_first_name, last_cleaning_date):
    """社員情報を更新"""

    user = User.objects.get(pk=user_id)
    cleaning_duty = CleaningDuty.objects.get(fk_user__pk=user_id)

    if user.username != employee_number:
        user.username = employee_number
    if user.last_name != employee_last_name:
        user.last_name = employee_last_name
        cleaning_duty.last_name = employee_last_name
    if user.first_name != employee_first_name:
        user.first_name = employee_first_name
        cleaning_duty.first_name = employee_first_name
    if cleaning_duty.last_cleaning_date != last_cleaning_date:
        cleaning_duty.last_cleaning_date = last_cleaning_date

    user.save()
    cleaning_duty.save()


def send_message_to_slack(message_type):
    """slackにメッセージを送信する"""

    cleaning_duties = CleaningDuty.objects.filter(duty_status=CleaningDuty.DUTY)
    next_cleaning_date = NextCleaningDate.objects.all().order_by('id')

    # 掃除当日でない場合はリマインドしない
    if message_type == CleaningStatus.CLEANING_START:
        now = datetime.datetime.now()
        if next_cleaning_date[0] != now.date():
            return

    # リマインドのタイミングで文言を出し分ける
    remind_text = u'@here: '
    if message_type == CleaningStatus.CLEANING_START:
        remind_text += u'掃除の時間になりました。\n'
    elif message_type == CleaningStatus.CLEANING_END:
        remind_text += u'掃除お疲れ様でした。\n次回の掃除実施日は%sです。\n' % next_cleaning_date[0].next_cleaning_date
    elif message_type == CleaningStatus.CLEANING_CHANGE:
        remind_text += u'掃除当番が変更されました。\n次回の掃除実施日は%sです。\n' % next_cleaning_date[0].next_cleaning_date
    elif message_type == CleaningStatus.CLEANING_DATE_CHANGE:
        remind_text += u'掃除日が変更されました。\n次回の掃除実施日は%sです。\n' % next_cleaning_date[0].next_cleaning_date
    else:
        remind_text += u'次回の掃除実施日は%sです。\n' % next_cleaning_date[0].next_cleaning_date
    remind_text += u'掃除機当番は、\n'
    for cleaning_duty in cleaning_duties:
        remind_text += u'%sさん\n' % cleaning_duty.get_employee_name()
    remind_text += u'です。ご協力よろしくお願いします。'

    # slackにPOSTする
    slack = slackweb.Slack(url=SLACK_CHANNEL)
    slack.notify(
        text=remind_text,
        username=u'cleaning_duty_bot',
        link_names=1
    )

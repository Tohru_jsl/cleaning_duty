from django.conf.urls import url
from cleaning_duty.apps.cleaning_duty.views import *


urlpatterns = [
    url(r'^$', cleaning_duty),                                                                          # メイン画面
    url(r'^cleaning_duty/list/get/$', get_cleaning_duty_list, name='get_cleaning_duty_list'),           # 掃除当番表を取得
    url(r'^cleaning_duty/save/$', save_cleaning_duty),                                                  # 掃除当番を保存
    url(r'^newest_cleaning_date/get/$', get_newest_cleaning_date),                                      # 最後の掃除実施日を取得
    url(r'^next_cleaning_date/save/$', save_next_cleaning_date),                                        # 次回掃除実施日を保存
    url(r'^next_cleaning_date/get/$', get_next_cleaning_date_str),                                      # 次回掃除実施日を取得
    url(r'^employee/save/$', save_employee),                                                            # 社員情報を保存
    url(r'^end_of_cleaning/$', end_of_cleaning),                                                        # 掃除完了
]
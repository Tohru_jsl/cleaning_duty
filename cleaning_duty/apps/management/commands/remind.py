from django.core.management.base import BaseCommand

from cleaning_duty.apps.cleaning_duty.query import send_message_to_slack


class Command(BaseCommand):
    """
    Slackに掃除のリマインドを送信するためのコマンド
    """

    def add_arguments(self, parser):
        parser.add_argument('remind_type', type=int)

    def handle(self, *args, **options):
        send_message_to_slack(options['remind_type'])

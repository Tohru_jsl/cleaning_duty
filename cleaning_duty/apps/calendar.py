import os

from datetime import date, timedelta
from httplib2 import Http
from oauth2client.service_account import ServiceAccountCredentials
from apiclient.discovery import build

from cleaning_duty.settings import BASE_DIR


class Calendar(object):
    """Googleカレンダーからデータを取得する"""

    CLIENT_SECRET_FILE = os.path.join(BASE_DIR, 'secret/client_secret.json')    # API用の認証JSON
    SCOPES = ['https://www.googleapis.com/auth/calendar.readonly']              # スコープ設定

    # 認証情報作成
    credentials = ServiceAccountCredentials.from_json_keyfile_name(CLIENT_SECRET_FILE, scopes=SCOPES)
    http_auth = credentials.authorize(Http())

    service = build("calendar", "v3", http=http_auth)   # API利用できる状態を作る

    @classmethod
    def get_holiday(cls, calendar_id):
        """休日を取得する"""
        today = date.today()
        dt_from = today.isoformat() + "T00:00:00.000000Z"
        dt_to = (today + timedelta(days=31)).isoformat() + "T00:00:00.000000Z"

        events = []
        events_results =\
            cls.service.events().list(
                calendarId=calendar_id,
                timeMin=dt_from,
                timeMax=dt_to,
                maxResults=50,
                singleEvents=True,
                orderBy="startTime"
            ).execute()

        for events_result in events_results.get('items', []):
            events.append(events_result.get('start').get('date'))

        return events

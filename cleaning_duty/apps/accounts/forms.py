from django import forms
from django.contrib.auth.forms import UserCreationForm
from cms.models.models import CleaningDuty


class UserRegisterForm(UserCreationForm):
    """user登録用フォーム"""
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def clean_password2(self):
        password1 = self.cleaned_data.get("password1")
        password2 = self.cleaned_data.get("password2")
        if password1 and password2 and password1 != password2:
            raise forms.ValidationError(
                self.error_messages['password_mismatch'],
                code='password_mismatch',
            )
        self.instance.username = self.cleaned_data.get('username')
        return password2

class CleaningDutyRegisterForm(forms.ModelForm):
    """cleaning_duty登録用フォーム"""
    last_name = forms.CharField(label=u'姓', widget=forms.TextInput())
    first_name = forms.CharField(label=u'名', widget=forms.TextInput())

    class Meta:
        model = CleaningDuty
        exclude = ('fk_user', 'last_cleaning_date', 'duty_status',)

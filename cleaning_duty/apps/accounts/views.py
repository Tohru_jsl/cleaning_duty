from django.shortcuts import render, redirect
from django.views.decorators.http import require_http_methods
from django.db import transaction

from cleaning_duty.apps.accounts.forms import UserRegisterForm, CleaningDutyRegisterForm
from cleaning_duty.apps.utils import DATE_MAX
from cms.models.models import CleaningDuty

# Create your views here.


def register(request):
    """新規アカウント作成画面表示"""

    context = {
        'user_form': UserRegisterForm(),
        'cleaning_duty_form': CleaningDutyRegisterForm(),
    }

    return render(request, 'accounts/register.html', context=context)


@require_http_methods(['POST'])
def save_register(request):
    """アカウントを保存する"""

    user_form = UserRegisterForm(request.POST)
    cleaning_duty_form = CleaningDutyRegisterForm(request.POST)
    is_not_cleaning_duty = request.POST.get('is_not_cleaning_duty')

    if user_form.is_valid() and cleaning_duty_form.is_valid():
        user = user_form.save(commit=False)
        user.first_name = cleaning_duty_form.cleaned_data['first_name']
        user.last_name = cleaning_duty_form.cleaned_data['last_name']
        user.save()

        # 掃除当番になり得る場合
        if not is_not_cleaning_duty:
            cleaning_duty = cleaning_duty_form.save(commit=False)
            cleaning_duty.fk_user = user
            cleaning_duty.last_cleaning_date = DATE_MAX
            cleaning_duty.duty_status = CleaningDuty.NONE
            cleaning_duty.save()

        return redirect('accounts:login')

    context = {
        'user_form': user_form,
        'cleaning_duty_form': cleaning_duty_form,
    }

    return render(request, 'accounts/register.html', context=context)

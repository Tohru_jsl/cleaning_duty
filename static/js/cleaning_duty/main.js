(function(){
    // Table作成用変数
    var _list_content;
    var _content_url;

    // 行データ格納用変数
    var data_table;
    var tr_element;
    var g_row_edit_data;

    // JSLの祝日リスト
    var _jsl_holidays;

    // 当番変更モーダル処理用フラグ
    var is_reload = true;

    // 当番ステータス
    var DUTY_STATUS_DUTY = 0;
    var DUTY_STATUS_NONE = 1;
    var DUTY_STATUS_DAY_OFF = 2;
    var DUTY_STATUS_TRANSFER = 3;

    // 当番ステータス名
    var DUTY_STATUS_NAME_LIST = [
        '掃除機当番',
        '---',
        '休暇・出張等',
        '客先'
    ];

    // Tableを作成
    cleaning_duty_list = {
        init_table: function(list_content, content_url) {
            _list_content = list_content;
            _content_url = content_url;
            var user_value_list = $('#user_name').attr('value').split(' ');
            var user_id = user_value_list[0];
            var is_staff = user_value_list[1];

            // dataTablesの言語設定
            var display_settings = {
                "oLanguage": {
                    "sProcessing": "処理中...",
                    "sLengthMenu": "_MENU_ 件表示",
                    "sZeroRecords": "データはありません。",
                    "sInfo": " _START_ 件 ～ _END_ 件を表示（全 _TOTAL_ 件中）",
                    "sInfoEmpty": " 0 件 〜 0 件を表示（全 0 件中）",
                    "sInfoFiltered": "（全 _MAX_ 件より抽出）",
                    "sInfoPostFix": "",
                    "sSearch": "絞込文字：",
                    "sUrl": "",
                    "oPaginate": {
                        "sFirst": "先頭",
                        "sPrevious": "前ページ",
                        "sNext": "次ページ",
                        "sLast": "最終"
                    }
                }
            };

            // dataTablesの列設定
            var column_defs = {
                "columnDefs": [
                    {
                        "targets": 0,
                        "data": "employee_number",
                        "width": 80,
                        "searchable": true,
                        "orderable": true
                    },
                    {
                        "targets": 1,
                        "data": "employee_full_name",
                        "searchable": true,
                        "orderable": true
                    },
                    {
                        "targets": 2,
                        "data": "last_cleaning_date",
                        "render": function (data, type, full, meta) {
                            var last_cleaning_date = full.last_cleaning_date;
                            if (last_cleaning_date.substring(0, 4) === '8888') {
                                last_cleaning_date = '-';
                            }

                            return last_cleaning_date;
                        },
                        "width": 110,
                        "searchable": true,
                        "orderable": true
                    },
                    {
                        "targets": 3,
                        "data": "duty_status",
                        "render": function (data, type, full, meta) {
                            var duty_status_list;

                            // ソート用に編集
                            duty_status_list = '<span hidden>' + full.duty_status + '</span>'
                            // プルダウンメニューを作成
                            duty_status_list += '<select id="duty_status' + full.cleaning_duty_id + '" ' +
                                                 'name="duty_status' + full.cleaning_duty_id + '" ' +
                                                 'style="width:120px;">';
                            for (var i = 0; i < DUTY_STATUS_NAME_LIST.length; i++) {
                                duty_status_list +=
                                    '<option value="' + i + '"' +
                                        ((i === full.duty_status) ? ' selected' : '') + '>' +
                                        DUTY_STATUS_NAME_LIST[i] +
                                    '</option>';
                            }
                            duty_status_list += '</select>';

                            return duty_status_list;
                        },
                        "width": 120,
                        "searchable": true,
                        "orderable": true
                    }
                ]
            };

            // 行の初期設定
            var created_rows = {
                "createdRow": function (row, data, index) {
                    if (data.duty_status === DUTY_STATUS_DUTY) {
                        $(row).addClass("duty");
                    }
                }
            };

            var settings = $.extend(display_settings, column_defs, created_rows);
            settings.sorting = [[3, "asc"], [2, "asc"],  [0, "asc"]];
            settings.dom = "<'my_pagenator'ipf>rt<'my_pagenator'lip>";
            settings.ajax = _content_url;

            data_table = $(_list_content).DataTable(settings)

            return data_table;
        },

        init_event: function(jsl_holidays) {
            _jsl_holidays = jsl_holidays;

            // 日付フォーマット
            function format_date(date) {
                var format = 'Y年M月d日';
                format = format.replace(/Y/g, date.substring(0, 4))
                               .replace(/M/g, date.substring(5, 7))
                               .replace(/d/g, date.substring(8, 10));

                return format;
            }

            // 指定した当番ステータスの社員を取得する
            function get_cleaning_duty_name_list(duty_status) {
                var cleaning_duty = [];
                var count = data_table.rows().count();
                for (var i = 0; i < count; i++) {
                    if (data_table.row(i).data().duty_status === duty_status) {
                        var last_cleaning_date = data_table.row(i).data().last_cleaning_date;
                        if (last_cleaning_date.substring(0, 4) === '8888') {
                            last_cleaning_date = '-';
                        }

                        cleaning_duty.push([
                            data_table.row(i).data().cleaning_duty_id,
                            data_table.row(i).data().employee_full_name,
                            last_cleaning_date,
                            data_table.row(i).data().employee_number
                        ]);
                    }
                }

                cleaning_duty.sort(function(a,b){
                    if(a[2] < b[2]) return -1;
                    if(a[2] > b[2]) return 1;
                    if(a[3] < b[3]) return -1;
                    if(a[3] > b[3]) return 1;
                    return 0;
                });

                return cleaning_duty;
            }

            // 必要な部分だけ画面更新
            function display_refresh() {
                $.get('next_cleaning_date/get/'
                ).done(function(res) {
                    $('.header h3').text('次回掃除実施日: ' + res.data);
                    $('#next_cleaning_date').val(res.data);
                }).fail(function() {
                    alert('次回掃除実施日の取得に失敗しました。');
                });
                data_table.ajax.reload();
            }

            // 同じ日に2回掃除完了しないように設定
            function get_newest_cleaning_date() {
                $.get('newest_cleaning_date/get/', function(data){
                    var date_list = data['data'].split('-');
                    var newest_cleaning_date = new Date(Number(date_list[0]), Number(date_list[1] - 1), Number(date_list[2]));

                    var today = new Date();
                    today.setHours(0);
                    today.setMinutes(0);
                    today.setSeconds(0);
                    today.setMilliseconds(0);

                    if (newest_cleaning_date.getTime() === today.getTime()) {
                        $('#end_of_cleaning').prop("disabled", true);
                    } else {
                        $('#end_of_cleaning').prop("disabled", false);
                    }
                });
            }

            // 各日時入力用フォームにdatepickerを設定
            $('.datepicker').bootstrapMaterialDatePicker({
                time: false,
                format: "YYYY-MM-DD",
                nowButton: true,
                clearButton: true,
                switchOnClick: true
            });

            // 当番変更確認用モーダル表示用の値設定
            $(document).on('change', "[id*=duty_status]", function() {
                tr_element = $(this).parent().parent()[0];
                g_row_edit_data = data_table.row(tr_element).data();
                var before_duty_status = g_row_edit_data.duty_status;
                var after_duty_status = Number($('[name=duty_status' + g_row_edit_data.cleaning_duty_id + ']').val());

                $('#duty_change_modal_employee_name').val(g_row_edit_data.cleaning_duty_id);
                $('#duty_change_modal_employee_name').text(g_row_edit_data.employee_full_name);
                $('#duty_change_modal_before_duty_status_name').text(DUTY_STATUS_NAME_LIST[before_duty_status]);
                $('#duty_change_modal_after_duty_status_name').text(DUTY_STATUS_NAME_LIST[after_duty_status]);

                // 掃除当番及び当番が可能な社員を取得
                var cleaning_duty_name_list = get_cleaning_duty_name_list(DUTY_STATUS_DUTY);
                var not_cleaning_duty_name_list = get_cleaning_duty_name_list(DUTY_STATUS_NONE);

                // 当番から変更した場合は、代わりの当番を設定させる
                if (before_duty_status === DUTY_STATUS_DUTY) {
                    if (not_cleaning_duty_name_list.length > 0) {
                        $('#duty_cancel').attr('style', 'display: block');
                        for (var i = 0; i < not_cleaning_duty_name_list.length; i++) {
                            $('#duty_select').append(
                                $('<option>').html(
                                    not_cleaning_duty_name_list[i][1] + ': ' +
                                    not_cleaning_duty_name_list[i][2]
                                ).val(not_cleaning_duty_name_list[i][0])
                            );
                        }
                    } else {
                        $('#result_text').text('代わりに当番可能な社員が見つかりませんでした。総務に相談してください。');
                        $('#result_modal_open').trigger('click');
                        return;
                    }
                } else {
                    $('#duty_cancel').attr('style', 'display: none');
                    $('#duty_select > option').remove();
                }

                // 当番が5人以上にならないようにする
                if (after_duty_status === DUTY_STATUS_DUTY && cleaning_duty_name_list.length >= 4) {
                    $('#result_text').text('当番が5人以上になります!!');
                    $('#result_modal_open').trigger('click');
                    return;
                }

                $('#duty_change_modal_open').trigger('click');
            });

            // 当番を変更
            $(document).on('click', "#duty_change_ok", function() {
                // モーダル終了時の画面更新をさせない
                is_reload = false;

                $.post('cleaning_duty/save/',
                {
                    'csrfmiddlewaretoken': $("input[name=csrfmiddlewaretoken]").val(),
                    'change_cleaning_duty_id': g_row_edit_data.cleaning_duty_id,
                    'change_duty_status': Number($('[name=duty_status' + g_row_edit_data.cleaning_duty_id + ']').val()),
                    'relief_cleaning_duty_id': Number($('[name=duty_select]').val()),
                    'is_slack_alert': $('#is_slack_alert_duty_change').prop('checked')
                }).done(function(res) {
                    if (res.data) {
                        $('#result_text').text('保存に成功しました。');
                    } else {
                        $('#result_text').text('保存に失敗しました。');
                    }
                }).fail(function() {
                    $('#result_text').text('保存に失敗しました。');
                }).always(function() {
                    $('#result_modal_open').trigger('click');
                });
            });

            // 当番変更しなかった場合は画面を更新
            $('#duty_change_modal').on('hidden.bs.modal', function () {
                if (is_reload) {
                    display_refresh();
                }
                is_reload = true;
            });

            // 掃除実施日保存確認
            $(document).on('click', "#save_next_cleaning_date", function() {
                $('#next_cleaning_date_modal_next_cleaning_date').text($('#next_cleaning_date').val());
            });
            // 掃除実施日を保存
            // $(document).on('click', "#save_next_cleaning_date", function() {
            $(document).on('click', "#save_next_cleaning_date_ok", function() {
                $.post('next_cleaning_date/save/',
                {
                    'csrfmiddlewaretoken': $("input[name=csrfmiddlewaretoken]").val(),
                    'next_cleaning_date': $('#next_cleaning_date').val(),
                    'is_slack_alert': $('#is_slack_alert_next_cleaning_date').prop('checked')
                }).done(function() {
                    $('#result_text').text('保存に成功しました。');
                }).fail(function() {
                    $('#result_text').text('保存に失敗しました。');
                }).always(function() {
                    $('#result_modal_open').trigger('click');
                });
            });

            // 掃除完了確認用モーダル表示用の値設定
            $(document).on('click', "#end_of_cleaning", function() {
                // 掃除完了日
                var cleaning_date = format_date($('#next_cleaning_date').val());
                $('#confirmation_modal_cleaning_date').text(cleaning_date);

                // 掃除当番を取得
                var cleaning_duty_name_list = get_cleaning_duty_name_list(DUTY_STATUS_DUTY);

                // 掃除当番を設定(4名まで)
                for (var i = 0; i < 4; i++) {
                    $('#confirmation_modal_cleaning_duty' + (i + 1)).text(cleaning_duty_name_list[i][1]);
                }
            });

            // 掃除完了処理及び次回掃除の設定
            $(document).on('click', "#confirmation_ok", function() {
                $.post('end_of_cleaning/',
                {
                    'csrfmiddlewaretoken': $("input[name=csrfmiddlewaretoken]").val(),
                    'cleaning_date': $('#next_cleaning_date').val(),
                    'is_slack_alert': $('#is_slack_alert_confirmation').prop('checked')
                }).done(function() {
                    $('#result_text').text('保存に成功しました。');
                }).fail(function() {
                    $('#result_text').text('保存に失敗しました。');
                }).always(function() {
                    $('#result_modal_open').trigger('click');
                });
            });

            // 対象の情報を取得してモーダルダイアログを開く
            $(document).on('click', "#edit_employee_get_row", function() {
                tr_element = $(this).parent().parent()[0];
                g_row_edit_data = data_table.row(tr_element).data();

                // ダイアログの初期値設定
                $('#edit_employee_number').val(g_row_edit_data.employee_number);
                $('#edit_employee_last_name').val(g_row_edit_data.employee_last_name);
                $('#edit_employee_first_name').val(g_row_edit_data.employee_first_name);
                $('#edit_last_cleaning_date').val(g_row_edit_data.last_cleaning_date);

                $('#edit_employee_modal_open').trigger('click');
            });

            // サーバに対象の情報を送信し社員情報を更新する
            $(document).on('click', "#edit_employee", function() {
                $.post('employee/save/',
                    {
                        'csrfmiddlewaretoken': $("input[name=csrfmiddlewaretoken]").val(),
                        'user_id': g_row_edit_data.user_id,
                        'employee_number': $('#edit_employee_number').val(),
                        'employee_last_name': $('#edit_employee_last_name').val(),
                        'employee_first_name': $('#edit_employee_first_name').val(),
                        'last_cleaning_date': $('#edit_last_cleaning_date').val(),
                        'is_slack_alert': ''
                    }).done(function() {
                        $('#result_text').text('保存に成功しました。');
                    }).fail(function() {
                        $('#result_text').text('保存に失敗しました。');
                    }).always(function() {
                        $('#result_modal_open').trigger('click');
                    });
            });

            // 情報保存後の画面をリフレッシュする
            $('#result_modal').on('hide.bs.modal', function () {
                display_refresh();
                get_newest_cleaning_date();
            });

            get_newest_cleaning_date();
        },

        get_jsl_holidays: function() {
            // JSLの休日を取得する
            return _jsl_holidays;
        }
    };
})();
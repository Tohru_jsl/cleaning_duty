from django.db import models


class BaseModel(models.Model):
    """
    ベースモデル
    """
    deleted = models.BooleanField(default=False, auto_created=True, null=False)
    created_at = models.DateTimeField(auto_now_add=True, null=False)
    updated_at = models.DateTimeField(auto_now=True, null=False)

    class Meta(object):
        abstract = True
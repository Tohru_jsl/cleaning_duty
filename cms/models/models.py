from django.db import models
from django.contrib.auth.models import User

from cms.models.base_models import BaseModel

from cleaning_duty.apps.utils import DATE_MAX


class CleaningDuty(BaseModel):
    """掃除当番表"""

    # 当番ステータス
    DUTY = 0        # 当番
    NONE = 1        # ---(当番可能)
    DAY_OFF = 2     # 休暇・出張等(当番不可・次回以降のステータスはDUTYかNONEに戻る)
    TRANSFER = 3    # 客先(当番不可・次回以降もこのまま)

    DUTY_STATUS_CHOICES = (
        (DUTY, u'掃除機当番'),
        (NONE, u'---'),
        (DAY_OFF, u'休暇・出張等'),
        (TRANSFER, u'客先'),
    )

    fk_user = models.ForeignKey(User, verbose_name=u'社員番号', related_name='cleaning_dutys')
    last_name = models.CharField(u'姓', max_length=10)
    first_name = models.CharField(u'名', max_length=10)
    last_cleaning_date = models.DateField(u'前回当番担当日', default=DATE_MAX)
    duty_status = models.IntegerField(u'当番ステータス', choices=DUTY_STATUS_CHOICES, default=NONE)

    def __str__(self):
        return self.last_name + self.first_name

    def get_employee_name(self):
        """社員名を取得"""

        return self.last_name + u'　' + self.first_name


class NextCleaningDate(BaseModel):
    """次回清掃実施日"""

    next_cleaning_date = models.DateField(u'次回清掃実施日', default=DATE_MAX)

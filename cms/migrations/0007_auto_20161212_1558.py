# -*- coding: utf-8 -*-
# Generated by Django 1.9.7 on 2016-12-12 06:58
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('cms', '0006_auto_20161212_1050'),
    ]

    operations = [
        migrations.AlterField(
            model_name='cleaningduty',
            name='duty_status',
            field=models.IntegerField(choices=[(0, '当番'), (1, 'なし'), (2, '休暇'), (3, '出向')], default=1, verbose_name='当番不可事由'),
        ),
    ]

from django.contrib import admin
from cms.models.models import CleaningDuty, NextCleaningDate


class CleaningDutyAdmin(admin.ModelAdmin):
    list_display = ('id', 'last_name', 'first_name', 'duty_status', 'last_cleaning_date',)
    list_display_links = ('id',)
admin.site.register(CleaningDuty, CleaningDutyAdmin)


class NextCleaningDateAdmin(admin.ModelAdmin):
    list_display = ('id', 'next_cleaning_date',)
    list_display_links = ('id', 'next_cleaning_date',)
admin.site.register(NextCleaningDate, NextCleaningDateAdmin)

